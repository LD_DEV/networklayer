//
//  ViewController.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import UIKit

struct User: Codable {
    let name: String
    let email: String
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataSource = AuthDataController(service: AuthService(), parser: AuthParser(), serializer: AuthSerializer())
        
        /*
        dataSource.signup(name: "Denis", email: "qwertyzz@gmail.com", password: "qwertyzz") { (session) in
            debugPrint()
        }
        */
        
        dataSource.login(email: "qwertyzz@gmail.com", password: "qwertyzz") { (session) in
            guard let session = session else { return }
            debugPrint("TOKEN: \(session.token)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

