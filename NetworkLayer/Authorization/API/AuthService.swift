//
//  AuthService.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

final class AuthService: APIService {
    
    func login(body: RequestBody, completion: @escaping APIServiceCompletionHandler) {
        execute(LoginRequest(), body: body, completion: completion)
    }
    
    func signup(body: RequestBody, completion: @escaping APIServiceCompletionHandler) {
        execute(SignupRequest(), body: body, completion: completion)
    }
}
