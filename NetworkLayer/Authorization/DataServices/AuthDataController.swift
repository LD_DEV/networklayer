//
//  AuthDataSource.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

final class AuthDataController {
    private let service: AuthService
    private let parser: AuthParser
    private let serializer: AuthSerializer
    
    init(service: AuthService, parser: AuthParser, serializer: AuthSerializer) {
        self.service = service
        self.parser = parser
        self.serializer = serializer
    }
    
    func login(email: String, password: String, completion: @escaping (Session?) -> Void) {
        let params = loginData(email: email, password: password)
        let serialized = serializer.serializeLoginData(loginData: params)!
        service.login(body: serialized) { (data, response, error) in
            let session = self.parser.parseLoginData(data: data)
            completion(session)
        }
    }
    
    func signup(name: String, email: String, password: String, completion: @escaping (Session?) -> Void) {
        let params = signupData(name: name, email: email, password: password)
        let serialized = serializer.serializeSignupData(signupData: params)!
        service.signup(body: serialized) { (data, response, error) in
            let session = self.parser.parseLoginData(data: data)
            completion(session)
        }
    }
    
    //MARK: - Private
    
    private func loginData(email: String, password: String) -> LoginData {
        let data = LoginData(email: email, password: password)
        return data
    }
    
    private func signupData(name: String, email: String, password: String) -> SignupData {
        let data = SignupData(name: name, email: email, password: password)
        return data
    }
}
