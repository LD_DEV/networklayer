//
//  LoginSerializer.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 28.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class AuthSerializer: Serializer {

    func serializeLoginData(loginData: LoginData) -> Data? {
        return super.serialize(loginData)
    }
    
    func serializeSignupData(signupData: SignupData) -> Data? {
        return super.serialize(signupData)
    }
}
