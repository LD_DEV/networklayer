//
//  AuthParser.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

final class AuthParser: Parser {
    
    /**
     Parse login data
     - parameters:
     - data: Data from response
     - returns:
     - a token string
     */
    
    func parseLoginData(data: Data?) -> Session? {
        return parse(data: data, type: Session.self)
    }
}
