//
//  LoginRequest.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 29.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class LoginRequest: Request {
    var method: RequestMethod {
        get { return .post }
    }
    
    var url: URL {
        get { return URL(string: APIService.baseURL + "/auth/login")! }
    }
}

class SignupRequest: Request {
    var method: RequestMethod {
        get { return .post }
    }
    
    var url: URL {
        get { return URL(string: APIService.baseURL + "/auth/register")! }
    }
}

class LogoutRequest: Request {
    var method: RequestMethod {
        get { return .post }
    }
    
    var url: URL {
        get { return URL(string: APIService.baseURL + "/auth/logout")! }
    }
}
