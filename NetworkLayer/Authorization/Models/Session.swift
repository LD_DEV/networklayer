//
//  Token.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 28.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class Session: Decodable {
    let token: String
    
    enum CodingKeys: String, CodingKey {
        case token = "Bearer"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.token = try container.decode(String.self, forKey: .token)
    }
}
