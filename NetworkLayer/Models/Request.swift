//
//  Request.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 29.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delte = "DELETE"
}

protocol Request {
    var method: RequestMethod {get}
    var url: URL {get}
}

protocol RequestWithParams: Request {
    var params: [String: Any] { set get }
}

class TaskRequest: RequestWithParams {
    var url: URL {
        get {
            let id = self._params["id"] as! String
            return URL(string: APIService.baseURL + "/task/\(id)")!
        }
    }
    
    var method: RequestMethod {
        get { return .get }
    }
    
    private var _params = [String: Any]()
    var params: [String : Any] {
        get {
            return _params
        }
        
        set {
            _params = newValue
        }
    }
}
