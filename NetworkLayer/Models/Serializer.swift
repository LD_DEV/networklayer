//
//  Serializer.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 28.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class Serializer {
    func serialize<T: Encodable>(_ value: T) -> Data? {
        do {
            return try JSONEncoder().encode(value)
        } catch {
            return nil
        }
    }
}
