//
//  APIService.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class APIService {
    static let baseURL = "http://207.154.216.162"
    
    typealias APIServiceCompletionHandler = ((Data?, URLResponse?, Error?) -> Void)
    typealias RequestBody = Data

    func execute(_ request: Request, body: RequestBody?, completion: @escaping APIServiceCompletionHandler) {
        var urlRequest = URLRequest(url: request.url)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpMethod = request.method.rawValue
        urlRequest.httpBody = body
        URLSession.shared.dataTask(with: urlRequest, completionHandler: completion).resume()
    }
}
