//
//  Parser.swift
//  NetworkLayer
//
//  Created by Denis Litvinskiy on 21.12.17.
//  Copyright © 2017 Denis Litvinskiy. All rights reserved.
//

import Foundation

class Parser {
    func parse<T: Decodable>(data: Data?, type: T.Type) -> T? {
        guard let data = data else { return nil }
        do {
            let parsed = try JSONDecoder().decode(type, from: data)
            return parsed
        } catch {
            return nil
        }
    }
}
